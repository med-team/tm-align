README.Debian for TMalign and TMScore
-------------------------------------

The homepage of TM-align (http://zhanglab.ccmb.med.umich.edu/TM-align/) says:

	TM-align is a computer algorithm for protein structure alignment using
	dynamic programming and TM-score rotation matrix. An optimal alignment
	between two proteins, as well as the TM-score, will be reported for each
	comparison. The value of TM-score lies in (0,1]. In general, a
	comparison of TM-score < 0.2 indicates that there is no similarity
	between two structures; a TM-score > 0.5 means the structures share the
	same fold. 

The homepage of TM-score (http://zhanglab.ccmb.med.umich.edu/TM-score/) says:

	TM-score is an algorithm to calculate the similarity of topologies of
	two protein structures. It can be exploited to quantitatively access the
	quality of protein structure predictions relative to native. Because
	TM-score weights the close matches stronger than the distant matches,
	TM-score is more sensitive than root-mean-square deviation (RMSD) (An
	illustative example can be found from here). A single score between
	(0,1] is assigned to each comparison. Based on statistics, if a
	template/model has a TM-score around or below 0.17, it means the
	prediction is nothing more than a random selection from PDB library.

The difference between both programs is explained as follows:

	What is the difference between TM-score and TM-align? The TM-score
	program is to compare two models based on their given and known residue
	equivalency. It is usually NOT applied to compare two proteins of
	different sequences. The TM-align is a structural alignment program for
	comparing two proteins whose sequences can be different. The TM-align
	will first find the best equivalent residues of two proteins based on
	the structure similarity and then output a TM-score. The TM-score values
	in both programs have the same definition.

 -- Andreas Tille <tille@debian.org>  Sun, 30 Jan 2011 22:49:21 +0100

