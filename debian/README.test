Notes on how this package can be tested.
────────────────────────────────────────

This package can be tested by executing

    sh run-unit-test

in order to confirm its integrity.

Package contains 2 executable files - TMalign and TMscore.
Both are called in `run-unit-test` script.

To test manually, cd to some dir, where you have write permissions and do
 
    cp -a /usr/share/doc/profbval/examples/* .
    gunzip 1ni7.pdb.gz 5eep.pdb.gz

Then run 
    TMalign 1ni7.pdb 5eep.pdb 
or
    TMscore 1ni7.pdb 5eep.pdb 

PDB structure files for this testsuite were picked randomly from RCSB PDB 
database [1], based on sequence similarity (first I picked random protein 
with 1 chain, then found similar structure with "Sequence Similarity Cutoff" 
at level 95%). As a result, they have TM-score metric > 0.8.
    
By default, TM-align page [2] suggests for comparison pair of protein 
structures with PDB ID "101m" and "1mba" - you can also run TMalign 
with these two structures and compare your results with those stated 
at Zhang Lab's site. 

References:
[1]: RCSB PDB database
     http://www.rcsb.org
[2]: TM-align page at Zhang Lab's site
     http://zhanglab.ccmb.med.umich.edu/TM-align/
